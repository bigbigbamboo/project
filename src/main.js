import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// 配置 混入
// this is romote 22223
//33
// 1111111
import '@/utils/mixin.js'  
import '@/utils/filters.js'  

// 进度条
// 112233
import NProgress from 'nprogress'
import 'nprogress/nprogress.css' //这个样式必须引入

// 配置 ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 配置 全局初始化样式
//11
import '@/assets/css/reset.scss';

import animated from 'animate.css' // npm install animate.css --save安装，在引入
Vue.use(animated)

// 富编辑器引入开始

import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor)
// 富编辑器引入结束

// 引入echarts开始
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

// 新用户引导
import Driver from 'driver.js'
import 'driver.js/dist/driver.min.css'
Vue.prototype.$driver = new Driver({
  doneBtnText: '完成', // Text on the final button
  closeBtnText: '关闭', // Text on the close button for this step
  stageBackground: '#fff', // Background color for the staged behind highlighted element
  nextBtnText: '下一步', // Next button text for this step
  prevBtnText: '上一步', // Previous button text for this step
})

//进度条
NProgress.inc(0.2)
NProgress.configure({ easing: 'ease', speed: 500, showSpinner: false })
router.beforeEach((to,from,next) => {
  NProgress.start()
  next()
})
router.afterEach(() => {
  NProgress.done()
})

//重复点击菜单报错
import Router from 'vue-router'
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}


import QRCode from 'qrcodejs2'


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
