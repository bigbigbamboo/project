






import axios from 'axios'
import qs from 'qs'

 



//商品类型
const goodsType = params => { 
    return axios.get('goods/type/index.php', {params:params}).then(res => res.data)
}

//属性删除
const attrdel = params => { 
    return axios.delete('goods/attr/delete.php', {params:params}).then(res => res.data)
}

//类型创建
const typeCj= params => {  
    return axios.post('goods/type/create.php', qs.stringify(params)).then(res => res.data)
}


// 商品属性
const goodsAttr = params => {  // params = {pagenum,pagesize,type_id}
    return axios.get('goods/attr/index.php', {params:params}).then(res => res.data)
}

//属性创建
const attrCreate = params => {  
    return axios.post('goods/attr/create.php', qs.stringify(params)).then(res => res.data)
}

//类型状态切换
const typeState = params => {  
    return axios.put('goods/type/state.php', qs.stringify(params)).then(res => res.data)
}

//类型删除
const typeDelete = params => {  
    return axios.delete('goods/type/delete.php',{params:params}).then(res => res.data)
}

 

export default {
    goodsType,
    goodsAttr,
    attrCreate,
    typeState,
    attrdel,
    typeDelete,
    typeCj,
 
}