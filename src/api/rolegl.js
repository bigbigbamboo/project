




import axios from 'axios'
import qs from 'qs'

 
//角色列表
const roleList = params => { 
    return axios.get('roles/index.php', {params:params}).then(res => res.data)
}
//角色创建
const roleCreate = params => {  
    return axios.post('roles/create.php', qs.stringify(params)).then(res => res.data)
}
//给角色分配权限
const rolePower = params => {  
    return axios.put('roles/assign.php', qs.stringify(params)).then(res => res.data)
}

//权限列表
const poewrList = params => { 
    return axios.get('auth/index.php', {params:params}).then(res => res.data)
}

export default {
    roleList,
    roleCreate,
    rolePower,
    poewrList
}