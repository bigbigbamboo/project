




import axios from 'axios'
import qs from 'qs'

 
//商品列表
const goodsList = params => { 
    return axios.get('goods/index.php', {params:params}).then(res => res.data)
}
//商品创建
const goodsCreate = params => {  
    return axios.post('goods/create.php', qs.stringify(params)).then(res => res.data)
}
//删除商品
const goodsDelete = params => {  
    return axios.delete('goods/delete.php', {params:params}).then(res => res.data)
}

//商品状态切换
const stateChange = params => {  
    return axios.put('goods/state.php', qs.stringify(params)).then(res => res.data)
}


export default {
    goodsList,
    goodsCreate,
    goodsDelete,
    stateChange
}