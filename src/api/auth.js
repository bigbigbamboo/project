




import axios from 'axios'
import qs from 'qs'

 
//权限列表
const authList = params => { 
    return axios.get('auth/index.php', {params:params}).then(res => res.data)
}
 

export default {
    authList
}