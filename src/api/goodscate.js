import axios from 'axios'
import qs from 'qs'

 
//商品分类列表
const goodsList = params => { 
    return axios.get('cate/index.php', {params:params}).then(res => res.data)
}
//商品分类创建
const goodsCreate = params => {  
    return axios.post('cate/create.php', qs.stringify(params)).then(res => res.data)
}


export default {
    goodsList,
    goodsCreate,
}