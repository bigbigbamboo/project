import axios from 'axios'
import qs from 'qs'

// //2.全局配置
// axios.defaults.baseURL = 'http://kg.zhaodashen.cn/v2/'
axios.defaults.baseURL = '/api/'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  let token = localStorage.getItem('token')
  config.headers['token'] = token
  // 拦截所有请求  发送之前就加一些东西
  config.headers['Content-Type'] = 'application/x-www-form-urlencoded'

  // 在发送请求之前做些什么
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  if (response.data.meta.msg == 'TOKEN有误')
  {
      Message({
          showClose: true,
          message: "糟糕TOKEN已过期请重新登录",
          type: "error"
      });
      return router.push({path:'/login'})
  }
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error,111);
});

// // export const getLoginApi = (paramsData) =>{
// //     return  axios.get('/users/login.php',{params:paramsData}).then(res=>res.data)
// // }

//   export const getLoginApi = getData => {    //postData
//      return axios.get('v1/public/login.jsp',{params:getData}).then(res => res.data)
//  }

//   export const getListApi = getData => {    //postData
//      return axios.get('v1/goods/index.jsp',{params:getData}).then(res => res.data)
//  }

// const postLoginApi = params => {  
//   return axios.post('users/login.php', qs.stringify(params)).then(res => res.data)
// }
 
import postLoginApi from "./login"
import admin from "./menu"
import usersApi from "./users"
import shopcateApi from "./goodscate"
import goodsAttrApi from "./goodsattr"
import goodsGlApi from "./goodsgl"
import orderGlApi from "./ordergl"
import usersGlApi from "./usergl"
import roleGlApi from "./rolegl"
import authApi from "./auth"
import typeCJ from "./typecreat"

export {
  postLoginApi,
  usersApi,
  admin,
  shopcateApi,
  goodsAttrApi,
  goodsGlApi,
  orderGlApi,
  usersGlApi,
  roleGlApi,
  authApi,
  typeCJ
}
 