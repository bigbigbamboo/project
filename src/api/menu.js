import axios from 'axios'
import qs from 'qs'

 

const menu = () => { 
    return axios.get('auth/menu.php', {}).then(res => res.data)
}


export default {
    menu
}