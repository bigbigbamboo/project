import axios from 'axios'
import qs from 'qs'

 
 
//类型创建
const tyCreate = params => {  
    return axios.post('goods/type/create.php', qs.stringify(params)).then(res => res.data)
}


export default {
    tyCreate
}