




import axios from 'axios'
import qs from 'qs'

 
//用户列表
const userList = params => { 
    return axios.get('users/index.php', {params:params}).then(res => res.data)
}
//商品创建
const usersCreate = params => {  
    return axios.post('users/create.php', qs.stringify(params)).then(res => res.data)
}
//分配角色
const roles = params => {  
    return axios.put('users/assign.php', qs.stringify(params)).then(res => res.data)
}
//角色
const nowroles = params => {  
    return axios.get('roles/index.php', {params:params}).then(res => res.data)
}

//用户创建
const peopleCreate = params => {  
    return axios.post('users/create.php', qs.stringify(params)).then(res => res.data)
}
//用户编辑  
const userBj = params => {  
    return axios.put('users/update.php', qs.stringify(params)).then(res => res.data)
}

//用户状态切换 
const stateChange = params => {  
    return axios.put('users/state.php', qs.stringify(params)).then(res => res.data)
}



export default {
    userList,
    usersCreate,
    roles,
    nowroles,
    peopleCreate,
    userBj,
    stateChange
}