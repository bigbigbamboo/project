




import axios from 'axios'
import qs from 'qs'

 
//订单管理  （订单列表）
const orderList = params => { 
    return axios.get('orders/index.php', {params:params}).then(res => res.data)
}
//订单统计
const orderTj = params => {  
    return axios.get('orders/total.php', qs.stringify(params)).then(res => res.data)
}

//订单删除  （订单列表）
const orderDel = params => { 
    return axios.delete('orders/delete.php', {params:params}).then(res => res.data)
}

export default {
    orderList,
    orderTj,
    orderDel
}