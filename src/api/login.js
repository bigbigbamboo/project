import axios from 'axios'
import qs from 'qs'

const login = params => {  
    return axios.post('users/login.php', qs.stringify(params)).then(res => res.data)
}


//  短息登录
const message = params => {  
    return axios.post('sms/login.php', qs.stringify(params)).then(res => res.data)
}

//  验证码
const mobile = params => {  
    return axios.post('sms/send.php', qs.stringify(params)).then(res => res.data)
}
//  二维码登录
const dl = params => {  
    return axios.post('qr/check.php', qs.stringify(params)).then(res => res.data)
}




export default {
    login,
    message,
    mobile,
    dl
}