import Vue from 'vue'
import VueRouter from 'vue-router'
 

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    component: () => import('../views/users/Login.vue')
  },
  {
    path: '/',
    component: () => import('../views/admin/Index.vue'),
    children:[
      {path: '/list',component: () => import('../views/list/List.vue')},
      {path: '/welcome', alias:'/', component: () => import('../views/admin/Welcome.vue'),meta:{Bname1:'后台首页',Bname2:'欢迎页'}},
      {path: '/history', alias:'/', component: () => import('../views/users/History.vue'),meta:{Bname1:'后台首页',Bname2:'访客记录'}},

      { path: 'goods/cate', component: () => import('../views/goods/cate/Index.vue'),meta:{Bname1:'商品分类',Bname2:'分类列表'}},
      { path: 'goods/cate/create', component: () => import('../views/goods/cate/Create.vue')},

      { path: 'goods/type', component: () => import('../views/goods/type/Index.vue'),meta:{Bname1:'商品属性',Bname2:'商品类型'}},
      { path: 'goods/type/create', component: () => import('../views/goods/type/Create.vue'),meta:{Bname1:'商品属性',Bname2:'类型创建'}},

      { path: 'goods/attr', component: () => import('../views/goods/attr/Index.vue'),meta:{Bname1:'商品属性',Bname2:'商品属性'}},
      { path: 'goods/attr/create', component: () => import('../views/goods/attr/Create.vue'),meta:{Bname1:'商品属性',Bname2:'属性创建'}},

      { path: 'goods', component: () => import('../views/goods/Index.vue'),meta:{Bname1:'商品管理',Bname2:'商品列表'}},
      { path: 'goods/create', component: () => import('../views/goods/Create.vue'),meta:{Bname1:'商品管理',Bname2:'商品创建'}},
      { path: 'goods/recycle', component: () => import('../views/goods/Recycle.vue'),meta:{Bname1:'商品管理',Bname2:'商品回收站'}},
      
      { path: 'orders', component: () => import('../views/orders/Index.vue'),meta:{Bname1:'订单管理',Bname2:'订单管理'}},
      { path: 'orders/recycle', component: () => import('../views/orders/Recycle.vue'),meta:{Bname1:'订单管理',Bname2:'订单回收站'}},
      { path: 'orders/total', component: () => import('../views/orders/Total.vue'),meta:{Bname1:'订单管理',Bname2:'订单统计'}},
 
      { path: 'users', component: () => import('../views/users/Index.vue'),meta:{Bname1:'用户管理',Bname2:'用户列表'}},
      { path: 'users/create', component: () => import('../views/users/Create.vue'),meta:{Bname1:'用户管理',Bname2:'用户创建'} },

      { path: 'roles', component: () => import('../views/roles/Index.vue'),meta:{Bname1:'角色管理',Bname2:'角色列表'}},
      { path: 'roles/create', component: () => import('../views/roles/Create.vue'),meta:{Bname1:'角色管理',Bname2:'角色创建'} },
      { path: 'auths', component: () => import('../views/auths/Index.vue') ,meta:{Bname1:'权限管理',Bname2:'权限列表'}}, 
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


// 全局前置守卫
// 在Router实例上进行守卫
router.beforeEach((to, from, next) => {
  next()
  // if (
    // to.path == '/login' || to.path == '/login1' || to.path == '/login2' || to.path == '/login3' ||
    // to.path == '/404'
    // to.path == '/login' || '/list' 
  // ) {
  //   next()
  // } else {
    let token = localStorage.getItem('token')
    if (!token) return next()
    
  // }
})

export default router
